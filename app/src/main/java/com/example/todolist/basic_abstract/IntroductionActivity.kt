package com.example.todolist.basic_abstract

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

class IntroductionActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_KEY_TITLE = "TITLE_NAME_INTRO"
        fun startActivity(context: Context, titleName: String) =
            context.startActivity(
                Intent(context, IntroductionActivity::class.java).also { myIntent ->
                    myIntent.putExtra(EXTRA_KEY_TITLE, titleName)
                }
            )
    }
}


