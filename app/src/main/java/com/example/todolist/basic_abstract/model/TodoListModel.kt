package com.example.todolist.basic_abstract.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TodoListModel(var isComplete: Boolean, var taskName: String) : Parcelable