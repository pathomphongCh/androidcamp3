package com.example.todolist.basic_abstract

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.R
import com.example.todolist.basic_abstract.model.TodoListModel

class TodoAdapter(private val listener: TodoClickListener) :
    RecyclerView.Adapter<TodoListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoListViewHolder =
        TodoListViewHolder(parent)

    override fun onBindViewHolder(holder: TodoListViewHolder, position: Int) {
        holder.bind(todoList[position], listener)
    }

    private val todoList: ArrayList<TodoListModel> = arrayListOf()
    override fun getItemCount(): Int {
        return todoList.count()
    }

    fun addListTask(task: TodoListModel) {
        todoList.add(task)
        notifyDataSetChanged()
    }


}


class TodoListViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_todo, parent, false)
) {
    private val taskName: TextView = itemView.findViewById(R.id.task)
    private val taskCheckBox: CheckBox = itemView.findViewById(R.id.cbTask)

    fun bind(model: TodoListModel, listener: TodoClickListener) {
        taskName.text = model.taskName
        itemView.setOnClickListener {
            (taskName.text.toString())
        }
        taskCheckBox.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
                println("pathomphong > $isChecked")
            }
        })
        itemView.visibility = if (model.isComplete) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
}

interface TodoClickListener {
    fun onItemClick(taskName: String)
}
