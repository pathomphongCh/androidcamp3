package com.example.todolist.basic_abstract

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todolist.R
import com.example.todolist.basic_abstract.model.TodoListModel
import kotlinx.android.synthetic.main.activity_main.*

class TodoInputActivity : AppCompatActivity() {
    private lateinit var todoAdapter: TodoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setView()
    }

    private fun setView() {

        val listener = object : TodoClickListener {
            override fun onItemClick(taskName: String) {
                IntroductionActivity.startActivity(
                    this@TodoInputActivity,
                    taskName
                )
            }
        }
        todoAdapter = TodoAdapter(listener)

        rvTodo.adapter = todoAdapter
        rvTodo.layoutManager = LinearLayoutManager(this)
        rvTodo.itemAnimator = DefaultItemAnimator()

        buttonAdd.setOnClickListener {
            val todoModel: TodoListModel = getAddModel(inputText.text.toString())
            todoAdapter.addListTask(todoModel)
            inputText.text = null
            println("Pathomphong")
        }
    }

    private fun getAddModel(name: String): TodoListModel {
        return TodoListModel(false, name)
    }
}