package com.example.todolist.basic_api.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.todolist.R
import com.example.todolist.basic_api.data.BeerModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_beer.*


class BeerActivity: AppCompatActivity(), BeerInterface {
    private val presenter = BeerPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer)
        presenter.getBeerApi()
        setView()
    }

    private fun setView(){
        buttonBeerRandom.setOnClickListener {
            presenter.getBeerApi()
        }
    }

    override fun setBeer(beerItem: BeerModel){
        beerName.text = "${beerItem.name}(${beerItem.abv}%)"
        beerDetail.text = beerItem.descriptor
        beerAbv.text = beerItem.abv.toString()
        Picasso.get().load(beerItem.image_url).placeholder(R.mipmap.ic_launcher).into(beerImage)
    }

}