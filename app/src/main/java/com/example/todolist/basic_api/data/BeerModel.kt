package com.example.todolist.basic_api.data

import com.google.gson.annotations.SerializedName

data class BeerModel(
    @SerializedName("name")
    val name: String?,

    @SerializedName("description")
    val descriptor: String?,

    @SerializedName("image_url")
    val image_url: String?,

    @SerializedName("abv")
    val abv: Double?
)